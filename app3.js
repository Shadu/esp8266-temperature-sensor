const http = require('http');
const fs = require('fs');
const port = 80;
const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
var bodyParser = require('body-parser');
const { parseJSON } = require('jquery');
app.set('view engine', 'pug')
app.use(express.static(__dirname + '/site'));
app.use(bodyParser.json());
app.listen(80);
var data;

const WebSocket = require('ws');
var ws;
connect();
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost/SHT30';
var myVar;
var temp;
var humid;
let ts;
let date_ob;
let date;
let month;
let year;
let hours;
let minutes;
let seconds;
function addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
function connect() {
    ws = new WebSocket('ws://' + '192.168.1.122' + "/ws");
    ws.on('open', function open() {
        ws.send('0');
        myVar = setInterval(requestData, (1000 * 60 * 10));
    });
    ws.onclose = function (e) {
        //console.log('Connection closed');
        setTimeout(function () {
            connect();
        }, 1000);
    }
    ws.onmessage = function (e) {
        //console.log('Receiving');
        ts = Date.now();
        date_ob = new Date(ts);
        date = addZero(date_ob.getDate());
        month = addZero(date_ob.getMonth() + 1);
        year = addZero(date_ob.getFullYear());
        hours = addZero(date_ob.getHours());
        minutes = addZero(date_ob.getMinutes());
        seconds = addZero(date_ob.getSeconds());

        var dataStr = [];
        dataStr = e.data.split(";");
        temp = parseFloat(dataStr[0]);
        humid = parseFloat(dataStr[1]);
        MongoClient.connect(url, function (err, db) {

            db.collection('Sensor').insertOne({
                Date: date + "-" + month + "-" + year,
                Time: hours + ":" + minutes + ":" + seconds,
                Temperature: temp,
                Humidity: humid


            });
            /*var cursor = db.collection('Sensor').find();
            cursor.each(function (err, doc) {
                console.log(doc);
            });*/
            db.close();
        });
        //}
    }
}
function requestData() {
    //console.log(ws.readyState);
    if (ws.readyState == 1) {
        ws.send('0');
        //console.log('Requesting');
    }
}
app.post('/test', function (req, res) {
    var date = req.body.date;
    var time = req.body.time;
    console.log(date);
    console.log(time);
    var dataStr = [];
        dataStr = date.split(";");
        firstDate = (dataStr[0]);
        lastDate = (dataStr[1]);
        console.log(firstDate);
        console.log(lastDate);
        var timeStr = [];
        timeStr = time.split(";");
        firstTime = timeStr[0];
        lastTime = timeStr[1];
        console.log(firstTime);
        console.log(lastTime);
    var MongoClient = require('mongodb').MongoClient;
    var url = 'mongodb://localhost/SHT30';

    MongoClient.connect(url, function (err, db) {
        db.collection('Sensor').find({Date: {$gte: firstDate, $lte: lastDate}, Time: {$gte: firstTime, $lte: lastTime}}).toArray(function(err, doc){
                res.send(doc);
            console.log(date);
            console.log(doc[1]);
    });
    })

});

/*const server = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/html' })
    fs.readFile('/site/index.html', function (error, data) {
        if (error) {
            res.writeHead(404);
            res.write('Error: File Not Found');
        } else {
            res.write(data);
        }
        res.end();
    });
});
server.listen(3000, '127.0.0.1');
console.log('Listening');*/
app.get('/', function (req, res) {
    res.sendFile('/site/index.html');
});
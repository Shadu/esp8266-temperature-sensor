

#include <Arduino.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_GFX.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <WEMOS_SHT3X.h>
#include <ESP8266WiFi.h> //Wifi library
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>

/**********Define WIFI Connection**********/
#ifndef STASSID
#define STASSID "WirelessTop"
#define STAPSK "16051996"
#endif
/******************************************/
#define OLED_RESET 0 // GPIO0
#define updatePin 15
#define viewPin 2

Adafruit_SSD1306 display(OLED_RESET);
IPAddress ipAddress;
SHT3X sht30(0x45);
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
AsyncEventSource events("/events");

/**********Select to use defined Wifi connection**********/
const char *ssid = STASSID;
const char *password = STAPSK;
/*********************************************************/
bool view_debounce = false;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
const long interval = (1000 * 60 * 10);
unsigned long currentCount = 0;
unsigned long previousCount = 0;
const long intervalCount = 500;
int viewInfo = 0;
int humidity;
float cTemp;
unsigned long lastDebounce = 0;
unsigned long bounceTimer = 50;

String html_1 = R"=====(
  <!DOCTYPE HTML>
<html>
<head>
    <title>Temperature and Humidity Sensor Attic</title>
</head>
<style type=text/css>
    span.top {
        font-weight: bold;
        color: blue;
        font-size: 36px;
    }

    span.info {
        font-weight: bold;
        color: white;
        font-size: 36px;
    }

    span.temp {
        font-weight: bold;
        color: white;
        font-size: 36px;
    }

    p.humid {
        font-weight: bold;
        color: white;
        font-size: 36px;
    }
</style>
<body style=background-color:black;>
    </style>
    <p><span class=top>Attic</span></p>
    <p id='temp'><span class=temp>Temperature: C</span></p>
    <p id='humid'><span class=temp>Humidity: %</span></p>
</body>
<script type="text/javascript">
    var myVar;
    var ws;
    window.onload = function () {
        ws = new WebSocket('ws://' + window.location.hostname + "/ws");
        ws.onopen = function () {
            requestData();
        }
        myVar = setInterval(requestData, 5000);
        ws.onmessage = function (e) {
            var dataStr = [];
            dataStr = e.data.split(";");
            var temp = parseFloat(dataStr[0]);
            var humid = parseFloat(dataStr[1]);
            document.getElementById('temp').innerHTML = "<span class = 'temp'>Temperature: " + temp + "C</span>";
            document.getElementById('humid').innerHTML = "<span class = 'temp'>Humidity: " + humid + "%</span>";
        }
    }
    function requestData() {
        ws.send('0');
    }
    window.onclose = function () {
        clearInterval(myVar);
    }
</script>
</html>
)=====";

void infoUpdate(void)
{
  if (digitalRead(updatePin) == HIGH)
  {
    currentCount = millis();
    if (currentCount - previousCount > intervalCount)
    {
      updateSensor();
      update();
      previousCount = currentCount;
    }
  }
}

void ICACHE_RAM_ATTR viewUpdate(void)
{
  for (int i = 0; i < 1; i++)
  {
    if (digitalRead(viewPin) == HIGH && viewInfo == 0 && view_debounce == false && (millis() - lastDebounce) > bounceTimer)
    {
      viewInfo = 1;
      update();
      view_debounce = true;
      lastDebounce = millis();
    }
    else if (digitalRead(viewPin) == HIGH && viewInfo == 1 && view_debounce == false && (millis() - lastDebounce) > bounceTimer)
    {
      viewInfo = 0;
      update();
      view_debounce = true;
      lastDebounce = millis();
    }
    if (digitalRead(viewPin) == LOW && view_debounce == true && (millis() - lastDebounce) > bounceTimer)
    {
      view_debounce = false;
      lastDebounce = millis();
    }
  }
}

void updateSensor(void)
{
  sht30.get();
  cTemp = sht30.cTemp;
  humidity = sht30.humidity;
}

void update(void)
{
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.setTextColor(WHITE);
  if (viewInfo == 0)
  {
    display.println("Temp: ");
    display.setTextSize(2);
    display.print(cTemp, 1);
    display.println("C");
    display.setTextSize(1);
    display.println("Vocht: ");
    display.setTextSize(2);
    display.print(humidity);
    display.println("%");
  }
  else if (viewInfo == 1)
  {
    if (WiFi.status() == WL_CONNECTED)
    {
      display.println("Connected:");
      display.println(ssid);
      display.println("On IP: ");
      display.println(ipAddress);
    }
    else if (WiFi.status() != WL_CONNECTED)
    {
      display.println("Trying to connect.");
    }
  }
  display.display();
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
  switch (type)
  {
  case WS_EVT_CONNECT:
    Serial.println("Websocket client connection received");
    break;
  case WS_EVT_DATA:
    String t = String(cTemp, 1) + ";" + String(humidity);
    //ws.textAll(t); // send response to all clients
    client->text(t); //send response to specific client
    Serial.println(t);
    Serial.println();
    break;
  }
}

void setup()
{
  Serial.begin(9600);
  /*********Enable Wifi****************/
  //WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  /************************************/
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); //enable oled on address 0x3C
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(0, 0);
    display.setTextColor(WHITE);
    display.println("Connecting");
  }
  ipAddress = WiFi.localIP();
  pinMode(updatePin, INPUT);
  pinMode(viewPin, INPUT);
  ws.onEvent(onEvent);
  server.addHandler(&ws);
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/html", html_1);
  });
  attachInterrupt(digitalPinToInterrupt(viewPin), viewUpdate, CHANGE);
  server.begin();
}

void loop()
{
  updateSensor();
  infoUpdate();
  if (currentMillis - previousMillis > interval)
  {
    update();
    previousMillis = currentMillis;
  }
    currentMillis = millis();
}
